package com.ass;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class FirstNonRepeted {

	public static void main(String[] args) {
		
		String s1 = "pool";
		String s2 = "loop";
		
//First non repeated

		for (int i = 0; i < s1.length(); i++) {
			char ch = s1.charAt(i);
			if (s1.indexOf(ch) == s1.lastIndexOf(ch)) {
				System.out.println("The first non repeated character is = " + ch);
				break;
			}
		}
		System.out.println("========================================");

//Removing duplicates

		LinkedHashSet<Character> set = new LinkedHashSet<>();

		for (int i = 0; i < s1.length(); i++) {

			char charAt = s1.charAt(i);
//			System.out.println(charAt);

			boolean add = set.add(charAt);
//			System.out.println(add);

			//System.out.println(set);
		}
		System.out.println("after removing duplicates= " + set);
		System.out.println("=====================================");

	
	// Anagram or not
	
	if (s1.length() ==s2.length()) {
		
		char[] charArray = s1.toCharArray();
		char[] charArray2 = s2.toCharArray();
		
		Arrays.sort(charArray);
		Arrays.sort(charArray2);
		
		String string = Arrays.toString(charArray);
		String string2 = Arrays.toString(charArray);
		
//		System.out.println(string);
//		System.out.println(string2);
//		
		if (string.equals(string2)) {
			System.out.println("It is an Anagram");
		}
		else {
			System.out.println("It is not an Anagram");
		}
	}
	else {
		System.out.println("It is not an Anagram");
	}
}

}
