package com.ass;

import java.util.Arrays;

public class ArraySort {

	public static void main(String[] args) {
		// sorting of array

		int[] a = { 3, 6, 2, 8, 5 };
		String[] b = { "Apple", "Doll", "Fan", "Cat", "Ant" };
		System.out.println("Sorting of array");
		System.out.println("----------------");
		Arrays.sort(a);
//		System.out.println(a);

		String string = Arrays.toString(a);
		System.out.println(string);

		Arrays.sort(b);
//		System.out.println(b);

		String string3 = Arrays.toString(b);
		System.out.println(string3);
		
		
		
		
		// copy of array
		System.out.println("copy of array");
		System.out.println("-------------");
		int[] c = new int[a.length];
//		String string2 = Arrays.toString(a);
//		System.out.println(string2);
//
//		String string1 = Arrays.toString(b);
//		System.out.println(string1);

		for (int i = 0; i < a.length; i++) {
			c[i] = a[i];
		}

		System.out.println("a array= " + Arrays.toString(a));
		System.out.println("c array= " + Arrays.toString(c));

	}
}
